# 个人博客

#### 介绍
本博客系统主要使用java语言进行开发，使用springboot框架，采用MySQL数据库。采用简洁的前端页面，让使用者更加容易使用。

#### 软件架构
springboot、thyemleaf、Mysql


#### 安装教程

第一步、下载解压源码到本地文件夹

第二部、导入idea中
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/103950_194fbbf5_7890662.png "屏幕截图.png")
选择要导入的项目
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/104133_e18f043b_7890662.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/104211_eeacea65_7890662.png "屏幕截图.png")
接下来都是下一步不用修改配置
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/104311_2ebe0575_7890662.png "屏幕截图.png")

第三部、配置数据库
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/104443_622075e5_7890662.png "屏幕截图.png")

第四步、新建数据库
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/104653_d9e4e29d_7890662.png "屏幕截图.png")
注意：数据库名要与配置数据库名称一样
      默认字符集使用utf-8

第五步、启动项目
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/105033_51c2e3eb_7890662.png "屏幕截图.png")
启动项目后会自动在数据库中上成对应的数据库表

第六步：导入数据库
在数据库命令行页面中输入数据库插入语句
![输入图片说明](https://images.gitee.com/uploads/images/2021/0317/105330_24bc47d0_7890662.png "屏幕截图.png")

SQl语句：

```
INSERT INTO `t_user` VALUES (1, 'https://unsplash.it/100/100?image=1005', '2017-10-15 12:36:04', 'hh@163.com', '管理员', '96e79218965eb72c92a549dd5a330112', 1, '2017-10-15 12:36:23', 'admin');
```

第七步、登录后台
1. 游览器访问http://localhost:8080/admin
2. 输入用户名：admin 用户名密码：111111
3. 在管理员界面增加一些数据，如标签、分类、文章
4. 在管理员界面新增数据后就可以到前台页面查看，游览器访问 http://localhost:8008



#### 项目截图

![管理员登录页面](https://images.gitee.com/uploads/images/2021/0317/110103_d9b0b77f_7890662.png "屏幕截图.png")

![管理员主页](https://images.gitee.com/uploads/images/2021/0317/110136_a08719bf_7890662.png "屏幕截图.png")

![撰写博客页面](https://images.gitee.com/uploads/images/2021/0317/110904_ce83b263_7890662.png "屏幕截图.png")

![管理员分类](https://images.gitee.com/uploads/images/2021/0317/110946_966f1db4_7890662.png "屏幕截图.png")

![管理员标签](https://images.gitee.com/uploads/images/2021/0317/110956_6ee0a59f_7890662.png "屏幕截图.png")

![前端页面](https://images.gitee.com/uploads/images/2021/0317/111035_b722cd09_7890662.png "屏幕截图.png")

![前端分类](https://images.gitee.com/uploads/images/2021/0317/111051_18718f5d_7890662.png "屏幕截图.png")

![前端标签](https://images.gitee.com/uploads/images/2021/0317/111119_4df9ff51_7890662.png "屏幕截图.png")

![前度归档](https://images.gitee.com/uploads/images/2021/0317/111141_2fb6be2f_7890662.png "屏幕截图.png")


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
