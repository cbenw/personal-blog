package com.jax.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Author cbenw
 * @Date 2021-03-16 11:59
 * @Description: md5加密
 * @Version: 1.0
 */
public class MD5DUtils {

    public static void main(String[] args){
     System.out.println(code("111111"));
    }

    public static String code(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("Md5");
            md.update(str.getBytes());
            byte[]byteDigest = md.digest();
            int i ;
            StringBuffer buf = new StringBuffer();
            for (int offset = 0 ; offset <byteDigest.length; offset++){
                i = byteDigest[offset];
                if (i < 0)
                    i +=256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();

        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
            return null;

        }
    }
}
