package com.jax.util;


import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author cbenw
 * @Date 2021-03-16 12:15
 * @Description: 获取所有属性值为空属性名数组
 * @Version: 1.0
 */
public class MyBeanUtils {
    public static String[] getNullPropertyNames(Object source) {
        BeanWrapperImpl beanWrapper = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = beanWrapper.getPropertyDescriptors();
        List<String> nullPropertyNames = new ArrayList<>();
        for (PropertyDescriptor pd :pds){
            String propertyName = pd.getName();
            if (beanWrapper.getPropertyValue(propertyName)==null){
                nullPropertyNames.add(propertyName);
            }
        }
        return nullPropertyNames.toArray(new String[nullPropertyNames.size()]);


    }

}
