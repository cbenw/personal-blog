package com.jax;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @Author cbenw
 * @Date 2021-03-16 11:44
 * @Description: com.jax
 * @Version: 1.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(){}
    public NotFoundException(String message){super(message);}
    public NotFoundException(String message,Throwable cause){super(message,cause);}
}
