package com.jax.mapper;

import com.jax.pojo.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Author cbenw
 * @Date 2021-03-16 14:56
 * @Description: com.jax.mapper
 * @Version: 1.0
 */
public interface TypeMapper  extends JpaRepository<Type,Long>{

    Type findByName(String name);

    @Query("select  t from Type t")
    List<Type> findTop(Pageable pageable);
}
