package com.jax.mapper;

import com.jax.pojo.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

/**
 * @Author cbenw
 * @Date 2021-03-16 14:51
 * @Description: com.jax.mapper
 * @Version: 1.0
 */
//public interface TagMapper extends JpaRepository<Tag,Long>{
//
//    Tag findByName(String name);
//
//    @Query("select t from Tag  t")
//    List<Tag> findTop(Pageable pageable);
//}

public interface TagMapper extends JpaRepository<Tag,Long> {

    Tag findByName(String name);

    @Query("select t from Tag t")
    List<Tag> findTop(Pageable pageable);

}
