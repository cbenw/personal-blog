package com.jax.mapper;

import com.jax.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author cbenw
 * @Date 2021-03-16 15:00
 * @Description: com.jax.mapper
 * @Version: 1.0
 */
public interface UserMapper extends JpaRepository<User,Long> {

    User findByUsernameAndPassword(String username,String password);
}
