package com.jax.mapper;

import com.jax.pojo.Comment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author cbenw
 * @Date 2021-03-16 14:48
 * @Description: com.jax.mapper
 * @Version: 1.0
 */
public interface CommentMapper extends JpaRepository<Comment,Long>{
    List<Comment> findByBlogIdAndParentCommentNull(Long blogId, Sort sort);

}
