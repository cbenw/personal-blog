package com.jax.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @Author cbenw
 * @Date 2021-03-16 15:03
 * @Description: com.jax.interceptor
 * @Version: 1.0
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {


    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Objects handler) throws Exception{
        if (request.getSession().getAttribute("user")==null){
            response.sendRedirect("/admin");
            return false;
        }
        return true;

    }

}
