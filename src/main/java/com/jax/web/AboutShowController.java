package com.jax.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author cbenw
 * @Date 2021-03-16 17:59
 * @Description: com.jax.web
 * @Version: 1.0
 */
@Controller
public class AboutShowController {


    @GetMapping("/about")
    public String about(){
        return "about";
    }
}
