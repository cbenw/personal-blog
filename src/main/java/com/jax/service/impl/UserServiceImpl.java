package com.jax.service.impl;

import com.jax.mapper.UserMapper;
import com.jax.pojo.User;
import com.jax.service.UserService;
import com.jax.util.MD5DUtils;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author cbenw
 * @Date 2021-03-16 17:12
 * @Description: com.jax.service.impl
 * @Version: 1.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User checkUser(String name, String password) {
        User user = userMapper.findByUsernameAndPassword(name, MD5DUtils.code(password));
        return user;
    }
}
