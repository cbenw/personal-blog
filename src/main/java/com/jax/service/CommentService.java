package com.jax.service;

import com.jax.pojo.Comment;

import java.util.List;

/**
 * @Author cbenw
 * @Date 2021-03-16 16:00
 * @Description: com.jax.service
 * @Version: 1.0
 */
public interface CommentService {

    List<Comment> listCommentByBlogId(Long blogId);

    Comment saveComment(Comment comment);
}
