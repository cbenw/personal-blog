package com.jax.service;

import com.jax.pojo.User;

/**
 * @Author cbenw
 * @Date 2021-03-16 17:11
 * @Description: com.jax.service
 * @Version: 1.0
 */
public interface UserService {

    User checkUser(String name, String password);
}
