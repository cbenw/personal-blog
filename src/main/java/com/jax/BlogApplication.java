package com.jax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author cbenw
 * @Date 2021-03-16 11:25
 * @Description: com.jax
 * @Version: 1.0
 */
@SpringBootApplication
public class BlogApplication {
    public static void main(String[] args){
        SpringApplication.run(BlogApplication.class,args);
    }
}
